#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from models import Accessories, Categories


class AccessoriesUpdateForm(forms.ModelForm):
    class Meta:
        model = Accessories
        fields = [
            'title',
            'stock_code',
            'tags',
            'sales_price',
            'purchase_price',
            'summary',
        ]

        widgets = {
            'title': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Title',
                    'required': 'true',
                }),
            'stock_code': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Stock Code',
                    'required': 'true',
                }),
            'tags': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'data-role': 'tagsinput',
                    'placeholder': 'Tags',
                    'required': 'true',
                }),
            'sales_price': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Sales Price',
                    'required': 'true',
                }),
            'purchase_price': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Purchase Price',
                    'required': 'true',
                }),
            'summary': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Summary'
                })
        }

    def clean_tags(self):
        tags = self.cleaned_data['tags']
        tags = str(tags).lower()
        return tags

    def clean_stock_code(self):
        stock_code = self.cleaned_data['stock_code']
        stock_code = str(stock_code).replace(' ', '').replace('-', '')
        return stock_code
