# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.urls import reverse


IMAGE_STATUS = (
    (1, 'Local'),
    (2, 'Url'),
    (3, 'Instagram')
)

SOCIALS_PLATFORMS = (
    (1, 'Instagram'),
    (2, 'Facebook'),
    (3, 'Twitter')
)


class Categories(models.Model):
    category = models.CharField(max_length=50, verbose_name='Category')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.category

    def get_details_url(self):
        return reverse('Accessories:categories_details', kwargs={'ids': self.id})

    @classmethod
    def get_or_create(cls, category):
        categories, created = cls.objects.get_or_create(category=category)

        if not categories.is_active:
            return None

        return categories


class Accessories(models.Model):
    title = models.CharField(max_length=255, verbose_name='Title')
    stock_code = models.CharField(max_length=10, unique=True, verbose_name='Stock Code')
    tags = models.CharField(max_length=255, verbose_name='Tags')
    categories = models.ForeignKey(Categories, blank=True, null=True)
    sales_price = models.FloatField(blank=True, null=True,verbose_name='Sales Price')
    purchase_price = models.FloatField(blank=True, null=True,verbose_name='Purchase Price')
    url = models.CharField(max_length=255, unique=True, verbose_name='Url')
    summary = models.CharField(max_length=255, verbose_name='Summary')
    created_at = models.DateField(auto_now_add=True, verbose_name='Created Date')
    updated_at = models.DateField(auto_now_add=True, verbose_name='Updated Date')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    @classmethod
    def get_or_create(cls, stock_code):
        accessories, created = cls.objects.get_or_create(stock_code=stock_code)

        return accessories


class Images(models.Model):
    filename = models.CharField(max_length=255, default="", verbose_name='Filename')
    url = models.URLField(max_length=500, verbose_name='Image')
    status = models.IntegerField(choices=IMAGE_STATUS, default=1, verbose_name='Status')
    shared_on_facebook = models.IntegerField(default=0, verbose_name='Shared on Facebook')
    shared_on_instagram = models.IntegerField(default=0, verbose_name='Shared on Instagram')
    shared_on_twitter = models.IntegerField(default=0, verbose_name='Shared on Twitter')
    crop_top = models.IntegerField(default=270, verbose_name='Crop-Top')
    images_id = models.IntegerField(default=0, verbose_name='Images ID')
    original_url = models.URLField(default='plugins/images/profile-logo.png', verbose_name='Original Image')
    is_checked = models.BooleanField(default=False)
    accessories = models.ForeignKey(Accessories)

    def __str__(self):
        return self.url

    def get_details_url(self):
        return reverse('Accessories:accessories_details', kwargs={'ids': self.id})

    def get_update_url(self):
        return reverse('Accessories:accessories_update', kwargs={'ids': self.accessories.id})

    def get_delete_url(self):
        return reverse('Accessories:accessories_delete', kwargs={'ids': self.id})

    def get_checked_url(self):
        return reverse('Accessories:accessories_checked', kwargs={'ids': self.id})

    def get_original_details_url(self):
        return reverse('Accessories:accessories_original_details', kwargs={'ids': self.id})

    @classmethod
    def get_or_create(cls, url, accessories, original_url, status=2, images_id=0):
        images, created = \
            cls.objects.get_or_create(url=url, status=status, images_id=images_id, original_url=original_url,
                                      accessories=accessories)

        return images


class Posts(models.Model):
    images = models.ForeignKey(Images)
    status = models.IntegerField(choices=SOCIALS_PLATFORMS, default=1, verbose_name='Social Platforms')
    created_at = models.DateField(auto_now_add=True, verbose_name='Created Date')

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.images.url, self.status, self.created_at)
