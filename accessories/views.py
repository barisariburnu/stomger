#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from stomger.cronjobs import share_on
from django.shortcuts import get_object_or_404, render, redirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from models import Accessories, Images
from accessories.forms import AccessoriesUpdateForm
from pure_pagination import Paginator
from stomger.utils import Utils
from datetime import datetime

reload(sys)
sys.setdefaultencoding('utf-8')


@login_required
def accessories_list(request):
    page = request.GET.get('page', 1)

    images = Images.objects.filter(status=3).order_by('accessories__id')
    p = Paginator(images, request=request, per_page=12)
    paginator = p.page(page)

    results = {'title': 'Accessories', 'paginator': paginator}

    return render(request, 'accessories_list.html', results)


@login_required
def accessories_details(request, ids):
    try:
        image = get_object_or_404(Images, id=ids)
        image.original = False

        results = {'image': image}
    except Exception as ex:
        print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.accessories.details')
        return Http404()

    return render(request, 'accessories_details.html', results)


@login_required
def accessories_original_details(request, ids):
    try:
        image = get_object_or_404(Images, id=ids)
        image.url = image.original_url
        image.original = True

        results = {'image': image}
    except Exception as ex:
        print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.accessories.details')
        return Http404()

    return render(request, 'accessories_details.html', results)


@login_required
def accessories_update(request, ids):
    if not request.user.is_authenticated():
        return Http404()

    accessory = get_object_or_404(Accessories, id=ids)
    form = AccessoriesUpdateForm(request.POST or None, instance=accessory)

    if form.is_valid():
        result = form.save(commit=False)
        result.updated_at = datetime.now()
        result.is_active = False
        result.save()

        # Not working on heroku server
        # Photo re-create and upload
        # images = Images.objects.filter(id=31965)
        # for image in images:
        #     utils = Utils(image)
        #     utils.delete_from_dropbox()
        #
        #     result = utils.create_instagram_image()
        #     image.url = result
        #     image.save()

        messages.success(request, 'The record was successfully updated')
        return redirect('Accessories:accessories_list')

    results = {'form': form}
    return render(request, 'accessories_update.html', results)


@login_required
def accessories_delete(request, ids):
    if not request.user.is_authenticated():
        return Http404()

    image = get_object_or_404(Images, id=ids)
    pure_image = get_object_or_404(Images, id=image.images_id)

    accessories_id = image.accessories.id

    # Remove photo from dropbox
    utils = Utils(image)
    utils.delete_from_dropbox()

    image.delete()
    pure_image.delete()

    count = Images.objects.filter(accessories__id=accessories_id).all().count()
    if count == 0:
        image.accessories.delete()

    messages.success(request, 'The record was successfully deleted')
    return redirect('Accessories:incomplete_list')


@login_required
def accessories_checked(request, ids):
    if not request.user.is_authenticated():
        return Http404()

    try:
        image = get_object_or_404(Images, id=ids)
        pure_image = get_object_or_404(Images, id=image.images_id)

        if image.is_checked:
            image.is_checked = False
            pure_image.is_checked = False
            message = 'The record was successfully unchecked'
        else:
            image.is_checked = True
            pure_image.is_checked = True
            message = 'The record was successfully checked'

            # # Not working on heroku server
            # # Remove photo on dropbox
            # utils = Utils(image)
            # utils.delete_from_dropbox()
            #
            # # Upload photo to dropbox
            # result = utils.create_instagram_image()
            # image.url = result

        image.save()
        pure_image.save()

        results = {'image': image}
    except:
        return Http404()

    messages.success(request, message)
    return render(request, 'accessories_details.html', results)


@login_required
def incomplete_list(request):
    page = request.GET.get('page', 1)

    images = Images.objects.filter(status=3, accessories__is_active=True, is_checked=False).order_by('accessories__id')
    p = Paginator(images, request=request, per_page=12)
    paginator = p.page(page)

    results = {'title': 'Accessories', 'paginator': paginator}

    return render(request, 'accessories_list.html', results)


@login_required
def complete_list(request):
    page = request.GET.get('page', 1)

    images = Images.objects.filter(status=3, accessories__is_active=True, is_checked=True).order_by('accessories__id')

    p = Paginator(images, request=request, per_page=12)
    paginator = p.page(page)

    results = {'title': 'Accessories', 'paginator': paginator}

    return render(request, 'accessories_list.html', results)


@login_required
def share_photo(request):
    share_on()
    return render(request, 'dashboard.html')
