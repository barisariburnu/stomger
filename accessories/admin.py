# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models import Accessories, Categories, Images


class AccessoriesAdmin(admin.ModelAdmin):
    list_display = ['stock_code', 'title', 'categories']
    list_filter = ['categories']
    search_fields = ['stock_code', 'title']

    class Meta:
        model = Accessories


class CategoriesAdmin(admin.ModelAdmin):
    list_display = ['id', 'category', 'is_active']
    list_filter = ['is_active']
    search_fields = ['category']

    class Meta:
        model = Categories


class ImagesAdmin(admin.ModelAdmin):
    list_display = ['url', 'accessories', 'status', 'shared_on_facebook', 'shared_on_instagram', 'shared_on_twitter']
    list_filter = ['accessories', 'status']
    search_fields = ['url']

    class Meta:
        model = Categories


admin.site.register(Accessories, AccessoriesAdmin)
admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Images, ImagesAdmin)
