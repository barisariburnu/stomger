# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-22 18:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accessories', '0004_auto_20170712_2207'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='images',
            name='shared_count',
        ),
        migrations.AddField(
            model_name='images',
            name='shared_on_facebook',
            field=models.IntegerField(default=0, verbose_name='Shared on Facebook'),
        ),
        migrations.AddField(
            model_name='images',
            name='shared_on_instagram',
            field=models.IntegerField(default=0, verbose_name='Shared on Instagram'),
        ),
        migrations.AddField(
            model_name='images',
            name='shared_on_twitter',
            field=models.IntegerField(default=0, verbose_name='Shared on Twitter'),
        ),
    ]
