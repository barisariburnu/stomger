#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from accessories import views

app_name = 'Accessories'

urlpatterns = [
    url(r'^$', views.accessories_list, name='accessories_list'),
    url(r'^(?P<ids>\d+)/$', views.accessories_details, name='accessories_details'),
    url(r'^(?P<ids>\d+)/original', views.accessories_original_details, name='accessories_original_details'),
    url(r'^(?P<ids>\d+)/update/$', views.accessories_update, name='accessories_update'),
    url(r'^(?P<ids>\d+)/delete/$', views.accessories_delete, name='accessories_delete'),
    url(r'^(?P<ids>\d+)/checked/$', views.accessories_checked, name='accessories_checked'),

    url(r'^incomplete$', views.incomplete_list, name='incomplete_list'),
    url(r'^complete$', views.complete_list, name='complete_list'),
    url(r'^share/photo$', views.share_photo, name='share_photo'),
]
