#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render, redirect, Http404, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required


@login_required
def dashboard(request):
    try:
        pass
    except:
        return Http404

    return render(request, 'dashboard.html')
