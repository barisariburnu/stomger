#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login, logout
from stomger.views import dashboard

app_name = 'Stomger'

urlpatterns = [
    url(r'^$', login, {'template_name': 'login.html'}, name='login'),
    url(r'^admin/', admin.site.urls),
    url(r'^dashboard$', dashboard, name='dashboard'),
    url(r'^accounts/logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^accessories/', include('accessories.urls')),
]
