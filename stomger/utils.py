#!/usr/bin/env python
# -*- coding: utf-8 -*-

from stomger.settings.base import BASE_DIR, STATIC_ROOT, PHONE_NUMBER, DROPBOX, BUFFER
from PIL import Image, ImageFont, ImageDraw
from enum import Enum
import dropbox
import buffer
import uuid
import wget
import sys
import os

PRICE_FONT = ImageFont.truetype(os.path.join(STATIC_ROOT, 'assets', 'fonts', 'Fairview_Regular.otf'), 70)
STOCK_FONT = ImageFont.truetype(os.path.join(STATIC_ROOT, 'assets', 'fonts', 'Fairview_Regular.otf'), 50)
MASK_IMAGE = os.path.join(BASE_DIR, '../', 'accessories', 'images', 'mask.png')
TEMP_DIR = os.path.join(BASE_DIR, '../', 'accessories', 'temporary')
FILENAME_FORMAT = '{special}{stock_code}-{uuid}.{ext}'
BUFFER_CLIENT = buffer.Client(BUFFER['ACCESS_TOKEN'], {'base': 'https://api.bufferapp.com'})
DROPBOX_CLIENT = dropbox.Dropbox(DROPBOX['ACCESS_TOKEN'])


class SocialPlatforms(Enum):
    FACEBOOK = BUFFER['FACEBOOK']
    TWITTER = BUFFER['TWITTER']
    INSTAGRAM = BUFFER['INSTAGRAM']


class Utils(object):
    def __init__(self, image):
        self.image = image

        if not os.path.exists(TEMP_DIR):
            os.makedirs(TEMP_DIR)

    ''' Create masked photos to share in instagram '''

    def create_instagram_image(self):
        box = (0, self.image.crop_top, 1080, self.image.crop_top + 1080)
        original_url = self.image.original_url

        try:
            os.chdir(TEMP_DIR)

            # Download Image
            downloaded = wget.download(original_url)
            extension = downloaded.replace('//', '/').split('.')[-1:][0]

            # Crop Image
            cropped_filename = FILENAME_FORMAT.format(special='cropped-', stock_code=self.image.accessories.stock_code,
                                                      uuid=str(uuid.uuid4()), ext=extension)
            cropped = Image.open(downloaded)
            crop = cropped.crop(box)
            crop.save(cropped_filename)

            # Mask Image
            masked_filename = FILENAME_FORMAT.format(special='masked-', stock_code=self.image.accessories.stock_code,
                                                     uuid=str(uuid.uuid4()), ext=extension)
            masked = Image.open(cropped_filename)
            mask = Image.open(MASK_IMAGE)
            masked.paste(mask, (0, 0), mask)

            draw = ImageDraw.Draw(masked, 'RGBA')
            draw.text((50, 65), "{0:.2f} TL".format(self.image.accessories.purchase_price),
                      font=PRICE_FONT,
                      fill=(255, 255, 255, 255))
            draw.text((95, 1020), PHONE_NUMBER,
                      font=STOCK_FONT,
                      fill=(255, 255, 255, 255))
            draw.text((847, 1020), self.image.accessories.stock_code,
                      font=STOCK_FONT,
                      fill=(255, 255, 255, 255))

            masked.save(masked_filename)

            # Upload Image
            result = self.upload_to_dropbox(masked_filename)

            print 'Success! Stock Code: {stock_code} \t Link: {link}' \
                .format(stock_code=self.image.accessories.stock_code, link=result)
            return result
        except Exception as ex:
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.utils.create_instagram_image')
        finally:
            file_list = [f for f in os.listdir(TEMP_DIR)]
            for f in file_list:
                os.remove(f)

    ''' Returns lower tags  '''

    def lower_tags(self):
        listed = ['#{0}'.format(tag.lower().replace('#', '')) for tag in self.image.accessories.tags.split(',')]
        self.image.accessories.tags = ",".join(str(x) for x in listed)

        return " ".join(str(x) for x in listed)

    ''' Returns photos on social platforms '''

    def share_on_buffer(self, platform):
        try:
            tags = self.lower_tags()
            image_url = DROPBOX_CLIENT.files_get_temporary_link(self.image.filename).link

            # Photos to share in facebook
            if SocialPlatforms.FACEBOOK == platform:
                self.image.shared_on_facebook += 1
                message = 'WhatsApp Sipariş: {phone} | {title} {hashtag} {tags}' \
                    .format(phone=PHONE_NUMBER, title=self.image.accessories.title.title(),
                            hashtag='@hayaltakiaksesuar', tags=tags)

            # Photos to share in twitter
            elif SocialPlatforms.TWITTER == platform:
                self.image.shared_on_twitter += 1
                message = 'WhatsApp Sipariş: {phone} {hashtag} {tags}' \
                    .format(phone=PHONE_NUMBER, title=self.image.accessories.title.title(),
                            hashtag='@HayalAksesuar', tags=tags)
                while True:
                    if len(message) < 140:
                        break
                    message = ' '.join(message.split(' ')[:-1])

            # Photos to share in instagram
            else:
                self.image.shared_on_instagram += 1
                message = 'WhatsApp Sipariş: {phone} | {title} {hashtag} {tags}' \
                    .format(phone=PHONE_NUMBER, title=self.image.accessories.title.title(),
                            hashtag='@hayal.taki', tags=tags)

            user = BUFFER_CLIENT.user()

            options = {'body': {'media': {'photo': image_url}, 'now': True}}
            user.create_update(message, [platform], options)

            self.image.save()
        except Exception as ex:
            print(ex.message)
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.utils.share_on_buffer')

    ''' Upload photos to dropbox '''

    def upload_to_dropbox(self, path=None):
        try:
            if path:
                downloaded = path
                extension = path.split('.')[-1:][0]
            else:
                os.chdir(TEMP_DIR)
                downloaded = wget.download(self.image.url)
                path = os.path.join(TEMP_DIR, downloaded)
                extension = path.split('.')[-1:][0]

            filename = FILENAME_FORMAT.format(special='/Instagram/', stock_code=self.image.accessories.stock_code,
                                              uuid=str(uuid.uuid4()), ext=extension)

            with open(downloaded, "rb") as f:
                DROPBOX_CLIENT.files_upload(f.read(), filename)

            self.image.filename = filename
            self.image.url = DROPBOX_CLIENT.sharing_create_shared_link(self.image.filename, short_url=False) \
                .url.replace('dl=0', 'dl=1')
            self.image.save()

            print('{0} - {1} - {2}'.format(self.image.id, self.image.accessories.stock_code, self.image.url))
            os.remove(os.path.join(TEMP_DIR, downloaded))
            return self.image.url
        except Exception as ex:
            print(ex)
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.utils.upload_to_dropbox')

    ''' Remove photos from dropbox '''

    def delete_from_dropbox(self):
        try:
            if self.image.filename != '':
                DROPBOX_CLIENT.files_delete(self.image.filename)
        except Exception as ex:
            print(ex)
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.utils.delete_from_dropbox')

    ''' Shares photos from dropbox '''

    def shares_photo_from_dropbox(self):
        try:
            self.image.url = DROPBOX_CLIENT.sharing_create_shared_link(self.image.filename, short_url=False) \
                .url.replace('dl=0', 'dl=1')
            self.image.save()
            return self.image.url
        except Exception as ex:
            print(ex)
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.utils.shares_photo_from_dropbox')
