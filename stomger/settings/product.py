#!/usr/bin/env python
# -*- coding: utf-8 -*-

from stomger.settings.base import *
from decouple import config
import dj_database_url


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY', default='you-will-never-walk-alone')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('FALSE', default=False, cast=bool)

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': dj_database_url.config(
        default=config('DATABASE_URL', default="postgres://user:pwd@ec2-23-23-86-179.compute-1.amazonaws.com:5432/db")
    )
}
