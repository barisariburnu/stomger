#!/usr/bin/env python
# -*- coding: utf-8 -*-

from accessories.models import Images
from stomger.utils import Utils, SocialPlatforms
from django.db.models import Max
import sys


def create_masked_image():
    images = Images.objects.filter(status=2, images_id=-1)

    for image in images:
        try:
            utils = Utils(image)
            utils.create_instagram_image()
        except Exception as ex:
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.cronjobs.masked')


def share_on():
    is_all_shared_on_facebook = False
    is_all_shared_on_twitter = False
    is_all_shared_on_instagram = False

    images = Images.objects.filter(status=3, is_checked=True)

    # Finds the highest share of social platforms
    max_facebook = images.aggregate(Max('shared_on_facebook'))['shared_on_facebook__max']
    max_instagram = images.aggregate(Max('shared_on_instagram'))['shared_on_instagram__max']
    max_twitter = images.aggregate(Max('shared_on_twitter'))['shared_on_twitter__max']

    # Finds how many shares you have in the last round
    shared_facebook = images.filter(shared_on_facebook=max_facebook)
    shared_twitter = images.filter(shared_on_twitter=max_twitter)
    shared_instagram = images.filter(shared_on_instagram=max_instagram)

    if shared_facebook.count() == images.count():
        is_all_shared_on_facebook = True

    if shared_twitter.count() == images.count():
        is_all_shared_on_twitter = True

    if shared_instagram.count() == images.count():
        is_all_shared_on_instagram = True

    ''' Start of Shares '''

    # Facebook
    share_on_facebook(shared_max=max_facebook, images=images, status=is_all_shared_on_facebook)

    #######################

    # Twitter
    share_on_twitter(shared_max=max_twitter, images=images, status=is_all_shared_on_twitter)

    #######################

    # Instagram
    share_on_instagram(shared_max=max_instagram, images=images, status=is_all_shared_on_instagram)

    ''' End of Shares '''


def share_on_facebook(shared_max, images, status):
    try:
        if status:
            image = images.first()
        else:
            image = images.filter(shared_on_facebook__lt=shared_max).first()

        utils = Utils(image)
        utils.share_on_buffer(SocialPlatforms.FACEBOOK)
    except Exception as ex:
        print(ex.message)
        print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.cronjobs.share_on_facebook')


def share_on_twitter(shared_max, images, status):
    try:
        if status:
            image = images.first()
        else:
            image = images.filter(shared_on_twitter__lt=shared_max).first()

        utils = Utils(image)
        utils.share_on_buffer(SocialPlatforms.TWITTER)
    except Exception as ex:
        print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.cronjobs.share_on_twitter')


def share_on_instagram(shared_max, images, status):
    try:
        if status:
            image = images.first()
        else:
            image = images.filter(shared_on_instagram__lt=shared_max).first()

        utils = Utils(image)
        utils.share_on_buffer(SocialPlatforms.INSTAGRAM)
    except Exception as ex:
        print(ex)
        print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.cronjobs.share_on_instagram')


def upload_all_photos_on_dropbox():
    images = Images.objects.filter(status=3, filename='')

    for image in images:
        try:
            utils = Utils(image)
            utils.upload_to_dropbox()
        except Exception as ex:
            print(ex)
            print('{}'.format(sys.exc_info()[-1].tb_lineno), ex, 'stomger.cronjobs.upload_all_photos_on_dropbox')
